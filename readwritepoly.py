def write_poly(p):
    """
      >>> write_poly({'5': 1})
      '5'
      >>> write_poly({'x': 1})
      'x'
      >>> write_poly({'2x': 1})
      '2x'
      >>> write_poly({'3x': 1, '-7': 1})
      '3x - 7'
      >>> write_poly({'3x': 5, '-7x': 1, '2': 1})
      '3x^5 - 7x + 2'
    """
    poly = ''
    for i in range(len(p)):
        if i > 0 and i < len(p) and list(p.keys())[i][0] != '-':
            poly += ' + '
        if list(p.keys())[i][0] == '-':
            poly += ' - ' + list(p.keys())[i][1:]
        else:
            poly += list(p.keys())[i]
        if list(p.values())[i] > 1:
            poly += '^' + str(list(p.values())[i])
    return poly


def read_poly(poly_str):
    """
      >>> read_poly('5')
      {'5': 1}
      >>> read_poly('x')
      {'x': 1}
      >>> read_poly('2x')
      {'2x': 1}
      >>> read_poly('3x - 7')
      {'3x': 1, '-7': 1}
      >>> read_poly('3x^5 - 7x + 2')
      {'3x': 5, '-7x': 1, '2': 1}
    """
    poly_list = poly_str.split(' ')
    poly_dict = {}
    b = ''
    for i in range(len(poly_list)):
        if poly_list[i] != '+' and poly_list[i] != '-':
            if poly_list[i-1] == '-':
                b = '-'
            if '^' in poly_list[i]:
                a = poly_list[i].split('^')
                poly_dict[b + a[0]] = int(a[1])
            else:
                poly_dict[b + poly_list[i]] = 1
        b = ''
    return poly_dict

if __name__ == '__main__':
    import doctest
    doctest.testmod()
